
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response)=>response.json())
.then((json)=> console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(`
Title: ${json.title} 
Body: ${json.body}`));



fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Updated Title',
		body: 'Updated Body'
	}),

})
.then((response)=> response.json())
.then((json)=> console.log(json));
